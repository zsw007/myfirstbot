#include "../include/Macroboard.h"

Macroboard::Macroboard() : Board()
{
}

Macroboard::Macroboard(std::vector<int> cells) : Board(cells)
{
}

int Macroboard::get_winner() const
{
	if ((field_[0][0] == 1 || field_[0][0] == 2) &&
		field_[0][0] == field_[0][1] && field_[0][1] == field_[0][2])
	{
		return field_[0][0];
	}

	// Horizontal second row.
	if ((field_[1][0] == 1 || field_[1][0] == 2) &&
		field_[1][0] == field_[1][1] && field_[1][1] == field_[1][2])
	{
		return field_[1][0];
	}

	// Horizontal third row.
	if ((field_[2][0] == 1 || field_[2][0] == 2) &&
		field_[2][0] == field_[2][1] && field_[2][1] == field_[2][2])
	{
		return field_[2][0];
	}

	// Vertical first row.
	if ((field_[0][0] == 1 || field_[0][0] == 2) &&
		field_[0][0] == field_[1][0] && field_[1][0] == field_[2][0])
	{
		return field_[0][0];
	}

	// Vertical second row.
	if ((field_[0][1] == 1 || field_[0][1] == 2) &&
		field_[0][1] == field_[1][1] && field_[1][1] == field_[2][1])
	{
		return field_[0][1];
	}

	// Vertical third row.
	if ((field_[0][2] == 1 || field_[0][2] == 2) &&
		field_[0][2] == field_[1][2] && field_[1][2] == field_[2][2])
	{
		return field_[0][2];
	}

	// Diagonal left to right.
	if ((field_[0][0] == 1 || field_[0][0] == 2) &&
		field_[0][0] == field_[1][1] && field_[1][1] == field_[2][2])
	{
		return field_[0][0];
	}

	// Diagonal right to left.
	if ((field_[0][2] == 1 || field_[0][2] == 2) &&
		field_[0][2] == field_[1][1] && field_[1][1] == field_[2][0])
	{
		return field_[0][2];
	}

	// All fields taken and still no winner, so tie.
	if (count_free_cells() == 0)
	{
		return 0;
	}

	// Game not over yet.
	return -1;
}

void Macroboard::allow_play()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (field_[i][j] == 0)
			{
				field_[i][j] = -1;
			}
		}
	}
}

void Macroboard::allow_play(int row, int col)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (i == row && j == col)
			{
				field_[i][j] = -1;
			}
			else if (field_[i][j] == -1)
			{
				field_[i][j] = 0;
			}
		}
	}
}

int Macroboard::count_free_cells() const
{
	int free = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (can_make_move(i, j))
			{
				free++;
			}
		}
	}

	return free;
}

bool Macroboard::can_play_board(int row, int col) const
{
	return field_[row][col] == -1;
}

bool Macroboard::can_make_move(int row, int col) const
{
	return field_[row][col] == -1 || field_[row][col] == 0;
}
