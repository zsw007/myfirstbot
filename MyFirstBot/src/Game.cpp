#include "../include/Game.h"

Game::Game()
{
	bot_id_ = 0;
	//macroboard_ = Macroboard();
}

Move_t Game::calculate_best_move(int time_limit)
{
	Stopwatch stopwatch(time_limit);
	stopwatch.start();

	// Iterative deepening search to calculate as many best moves as possible.
	for (int depth = 0; depth >= 0; depth++)
	{
		if (!stopwatch.check())
		{
			stopwatch.stop();
			break;
		}

		for (int board_row = 0; board_row < 3; board_row++)
		{
			for (int board_col = 0; board_col < 3; board_col++)
			{
				if (can_play(board_row, board_col))
				{
					for (int row = 0; row < 3; row++)
					{
						for (int col = 0; col < 3; col++)
						{
							if (can_make_move(board_row, board_col, row, col))
							{
								Game look_ahead = *this;
								look_ahead.make_move(board_row, board_col, row, col, bot_id_);

								score_[board_row][board_col][row][col] = alphabeta(look_ahead, depth,
								                                                   std::numeric_limits<int>::min(), 
									                                               std::numeric_limits<int>::max(),
								                                                   opponent_id(bot_id_));
							}
						}
					}
				}
			}
		}
	}


	// Out of time, we'll now take the best move we got and make it.
	int max_score = std::numeric_limits<int>::min();
	for (int board_row = 0; board_row < 3; board_row++)
	{
		for (int board_col = 0; board_col < 3; board_col++)
		{
			if (can_play(board_row, board_col))
			{
				for (int row = 0; row < 3; row++)
				{
					for (int col = 0; col < 3; col++)
					{
						if (can_make_move(board_row, board_col, row, col))
						{
							max_score = std::max(max_score, score_[board_row][board_col][row][col]);
						}
					}
				}
			}
		}
	}

	// What if there are multiple best moves? We'll check all of them.
	std::vector<Move_t> moves;
	for (int board_row = 0; board_row < 3; board_row++)
	{
		for (int board_col = 0; board_col < 3; board_col++)
		{
			if (can_play(board_row, board_col))
			{
				for (int row = 0; row < 3; row++)
				{
					for (int col = 0; col < 3; col++)
					{
						if (can_make_move(board_row, board_col, row, col) &&
							score_[board_row][board_col][row][col] == max_score)
						{
							Move_t move;
							move.board_row = board_row;
							move.board_col = board_col;
							move.row = row;
							move.col = col;
							moves.push_back(move);
						}
					}
				}
			}
		}
	}

	return moves[rand() % moves.size()];
}

void Game::update_boards(std::string value)
{
	std::vector<std::string> raw = split(value, ',');
	std::vector<int> field = std::vector<int>(81);

	// Maps the vector of strings into a vector of integers, giving us an 81 cell field.
	std::transform(raw.begin(), raw.end(), field.begin(),
	               [](const std::string s)
	               {
		               return stoi(s);
	               });

	// We now want to transform the field into 3x3=9 Boards.
	for (int i = 0; i < 9; i++)
	{
		// The field is laid out as follow:
		//
		// 0  1  2    3  4  5    6  7  8
		// 9  10 11   12 13 14   15 16 17
		// 18 19 20   21 22 23   24 25 26
		//
		// 27 28 29   30 31 32   33 34 35
		// 36 37 38   39 40 41   42 43 44
		// 45 46 47   48 49 50   51 52 53
		//
		// 54 55 56   57 58 59   60 61 62
		// 63 64 65   66 67 68   69 70 71
		// 72 73 74   75 76 77   78 79 80
		std::vector<int> cells;

		// We want to select starting points at 0, 3, 6, 27, 30, 33, 54, 57, 60.
		int j = i * 3 + 18 * (i / 3);
		cells.push_back(field[j]);
		cells.push_back(field[j + 1]);
		cells.push_back(field[j + 2]);
		cells.push_back(field[j + 9]);
		cells.push_back(field[j + 10]);
		cells.push_back(field[j + 11]);
		cells.push_back(field[j + 18]);
		cells.push_back(field[j + 19]);
		cells.push_back(field[j + 20]);

		// The transformation as follows:
		// 0 1 2      0,0 0,1 0,2
		// 3 4 5  --> 1,0 1,1 1,2
		// 6 7 8      2,0 2,1 2,2
		//
		// i/3 = 0 from 0-2, 1 from 3-5, 2 from 6-8
		// i%3 = 0, 1, 2 alternating.
		Board board = Board(cells, i / 3, i % 3);
		boards_[i / 3][i % 3] = board;
	}
}

void Game::update_macroboard(std::string value)
{
	std::vector<std::string> raw = split(value, ',');
	std::vector<int> cells = std::vector<int>(9);

	// Maps the vector of strings into a vector of integers, giving us an 81 cell field.
	std::transform(raw.begin(), raw.end(), cells.begin(),
	               [](const std::string s)
	               {
		               return stoi(s);
	               });

	macroboard_ = Macroboard(cells);

	// We change ties to be represented by 3 instead of 0. This is because 0 can also be intrepreted
	// as a board where a move can still be made, which is ambiguous.
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (boards_[i][j].get_winner() == 0)
			{
				macroboard_.make_move(i, j, 3);
			}
		}
	}
}

void Game::make_move(int board_row, int board_col, int row, int col, int bot_id)
{
	boards_[board_row][board_col].make_move(row, col, bot_id);

	// If we finish the board after making the move, mark it on the macroboard.
	if (boards_[board_row][board_col].get_winner() != -1)
	{
		// Mark ties as 3 on the macroboard to disambiguate.
		int result = boards_[board_row][board_col].get_winner();
		if (result == 0)
		{
			result = 3;
		}

		macroboard_.make_move(board_row, board_col, result);
	}

	// Mark the playable moves for the next player.
	if (boards_[row][col].get_winner() != -1)
	{
		macroboard_.allow_play();
	}
	else
	{
		macroboard_.allow_play(row, col);
	}
}

bool Game::can_play(int row, int col) const
{
	return macroboard_.can_play_board(row, col);
}

bool Game::can_make_move(int board_row, int board_col, int row, int col) const
{
	return boards_[board_row][board_col].can_make_move(row, col);
}

bool Game::is_won() const
{
	return macroboard_.get_winner() == bot_id_;
}

bool Game::is_lost() const
{
	return macroboard_.get_winner() == opponent_id(bot_id_);
}

bool Game::is_tied() const
{
	return macroboard_.get_winner() == 0;
}

bool Game::is_game_over() const
{
	return macroboard_.get_winner() != -1;
}

Macroboard Game::get_macroboard() const
{
	return macroboard_;
}

int Game::get_bot_id() const
{
	return bot_id_;
}

void Game::set_bot_id(int bot_id)
{
	bot_id_ = bot_id;
}

int Game::count_board_effective_wins() const
{
	int count = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (boards_[i][j].get_winner() == bot_id_)
			{
				count++;
			}
			else if (boards_[i][j].get_winner() == opponent_id(bot_id_))
			{
				count--;
			}
		}
	}

	return count;
}

int Game::count_board_effective_can_wins() const
{
	int count = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			count += boards_[i][j].count_can_wins(bot_id_);
			count -= boards_[i][j].count_can_wins(opponent_id(bot_id_));
		}
	}

	return count;
}

int Game::count_board_effective_near_wins() const
{
	int count = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			count += boards_[i][j].count_near_wins(bot_id_);
			count -= boards_[i][j].count_near_wins(opponent_id(bot_id_));
		}
	}

	return count;
}

int Game::count_macroboard_effective_can_wins() const
{
	return macroboard_.count_can_wins(bot_id_)
		- macroboard_.count_can_wins(opponent_id(bot_id_));
}

int Game::count_macroboard_effective_near_wins() const
{
	return macroboard_.count_near_wins(bot_id_)
		- macroboard_.count_near_wins(opponent_id(bot_id_));
}

int Game::evalulate() const
{
	int weight = 0;

	if (is_won())
	{
		return std::numeric_limits<int>::max();
	}

	if (is_lost())
	{
		return std::numeric_limits<int>::min();
	}

	if (is_tied())
	{
		return 0;
	}

	weight += count_board_effective_can_wins() * 1;

	weight += count_board_effective_near_wins() * 10;

	weight += count_board_effective_wins() * 100;

	weight += count_macroboard_effective_can_wins() * 1000;

	weight += count_macroboard_effective_near_wins() * 10000;

	return weight;
}

int alphabeta(Game game, int depth, int alpha, int beta, int turn)
{
	if (depth == 0 || game.is_game_over())
	{
		return game.evalulate();
	}

	// If game.get_bot_id() = turn => is maximizing player.
	int value = game.get_bot_id() == turn ?
		            std::numeric_limits<int>::min() : std::numeric_limits<int>::max();

	for (int board_row = 0; board_row < 3; board_row++)
	{
		for (int board_col = 0; board_col < 3; board_col++)
		{
			if (game.can_play(board_row, board_col))
			{
				for (int row = 0; row < 3; row++)
				{
					for (int col = 0; col < 3; col++)
					{
						if (game.can_make_move(board_row, board_col, row, col))
						{
							Game look_ahead = game;
							look_ahead.make_move(board_row, board_col, row, col, turn);

							if (game.get_bot_id() == turn)
							{
								value = std::max(value,
								                 alphabeta(look_ahead, depth - 1, alpha, beta, opponent_id(turn)));
								alpha = std::max(alpha, value);
							}
							else
							{
								value = std::min(value,
								                 alphabeta(look_ahead, depth - 1, alpha, beta, opponent_id(turn)));
								beta = std::min(beta, value);
							}

							if (beta <= alpha)
							{
								// Break out of all 4 nested loop, since they actually
								// represent a single iteration through the child nodes.
								board_row = 3;
								board_col = 3;
								row = 3;
								break;
							}
						}
					}
				}
			}
		}
	}

	return value;
}


// http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim))
	{
		elems.push_back(item);
	}
	return elems;
}


// http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> split(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

inline int opponent_id(int bot_id)
{
	return 3 - bot_id;
}
