#include "../include/Board.h"

inline int calculateOpponentId(int botId)
{
	return 3 - botId;
}

Board simulateMove(Board board, int row, int col, int botId)
{
	board.make_move(row, col, botId);
	return board;
}

std::vector<std::vector<int>> calculateScore(Board board, int botId)
{
	std::vector<std::vector<int>> scoreField(3, std::vector<int>(3));
	int opponentId = calculateOpponentId(botId);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			// Wins in 0 moves much higher score than win in 1 move.
			if (board.get_winner() == botId)
			{
				scoreField[i][j] = int(pow(2, 10)) - 1;
			}
			else if (board.get_winner() == opponentId)
			{
				scoreField[i][j] = -(int(pow(2, 10)) - 1);
			}
			else if (board.can_make_move(i, j))
			{
				// Simulate a possible move being made here.
				Board possibleBoard = simulateMove(board, i, j, botId);

				// Calculate the score of making this move, assuming that the opponent goes next.
				int score = minimax(possibleBoard, botId, opponentId);

				scoreField[i][j] = score;
			}
		}
	}

	return scoreField;
}

int minimax(Board board, int botId, int turnPlayer, int depth)
{
	int opponentId = calculateOpponentId(botId);

	// Since there are alot more moves to make in this game_ than standard tic tac toe, with every
	// extra move we have to go (represented by depth), we are less and less sure that this move
	// accurately represents a move our opponent or we will make. So with every depth, we decrease
	// the amount, but we do it less and less so, since we can't be so sure that our move is still
	// bad enough to warrant decreasing by that much.
	// We use the power of 2 summation formula: 2^(n + 1) - 1, but we want it decreasing instead.
	int score = int(pow(2, 9 - depth)) - 1;

	// Tie, depth doesn't matter since it should all be the same.
	if (board.get_winner() == 0)
	{
		// We faster we tie the better, but tie is still undesirable.
		return 0;
	}

	// We win.
	if (board.get_winner() == botId)
	{
		// The faster we achieve victory the better.
		return score;
	}

	// We lose.
	if (board.get_winner() == opponentId)
	{
		// The slower we reach defeat the better.
		return -score;
	}

	// Otherwise, we keep trying other possible moves.
	std::vector<int> scores;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (board.can_make_move(i, j))
			{
				// A possible move to make by the current player.
				Board possibleBoard = simulateMove(board, i, j, turnPlayer);
				int newTurnPlayer = calculateOpponentId(turnPlayer);

				scores.push_back(minimax(possibleBoard, botId, newTurnPlayer, depth + 1));
			}
		}
	}

	// Maximize when it is our turn, minimize when it is opponent's turn because the opponent will
	// try to maximize as well. 
	if (turnPlayer == botId)
	{
		return *std::max_element(scores.begin(), scores.end());
	}

	return *std::min_element(scores.begin(), scores.end());
}

int minimax(Board board, int botId, int turnPlayer)
{
	return minimax(board, botId, turnPlayer, 0);
}

Board::Board()
{
	col_ = 0;
	row_ = 0;
}

Board::Board(std::vector<int> cells) : Board()
{
	// Convert 9 cells of 0 1 2 3 4 5 6 7 8 to a 3 by 3 field of
	// 0 1 2
	// 3 4 5
	// 6 7 8
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			field_[i][j] = cells[3 * i + j];
		}
	}
}

Board::Board(std::vector<int> cells, int row, int col) : Board(cells)
{
	row_ = row;
	col_ = col;
}

int Board::count_free_cells() const
{
	int free = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (can_make_move(i, j))
			{
				free++;
			}
		}
	}

	return free;
}

int Board::get_winner() const
{
	// Only 8 ways to win.
	// 0,0 0,1 0,2
	// 1,0 1,1 1,2
	// 2,0 2,1 2,2

	// Horizontal first row.
	if (field_[0][0] > 0 && field_[0][0] == field_[0][1] && field_[0][1] == field_[0][2])
	{
		return field_[0][0];
	}

	// Horizontal second row.
	if (field_[1][0] > 0 && field_[1][0] == field_[1][1] && field_[1][1] == field_[1][2])
	{
		return field_[1][0];
	}

	// Horizontal third row.
	if (field_[2][0] > 0 && field_[2][0] == field_[2][1] && field_[2][1] == field_[2][2])
	{
		return field_[2][0];
	}

	// Vertical first row.
	if (field_[0][0] > 0 && field_[0][0] == field_[1][0] && field_[1][0] == field_[2][0])
	{
		return field_[0][0];
	}

	// Vertical second row.
	if (field_[0][1] > 0 && field_[0][1] == field_[1][1] && field_[1][1] == field_[2][1])
	{
		return field_[0][1];
	}

	// Vertical third row.
	if (field_[0][2] > 0 && field_[0][2] == field_[1][2] && field_[1][2] == field_[2][2])
	{
		return field_[0][2];
	}

	// Diagonal left to right.
	if (field_[0][0] > 0 && field_[0][0] == field_[1][1] && field_[1][1] == field_[2][2])
	{
		return field_[0][0];
	}

	// Diagonal right to left.
	if (field_[0][2] > 0 && field_[0][2] == field_[1][1] && field_[1][1] == field_[2][0])
	{
		return field_[0][2];
	}

	// All fields taken and still no winner, so tie.
	if (count_free_cells() == 0)
	{
		return 0;
	}

	// Game not over yet.
	return -1;
}

int Board::count_can_wins(int bot_id) const
{
	// 0,0 0,1 0,2
	// 1,0 1,1 1,2
	// 2,0 2,1 2,2
	int count = 0;

	if (field_[0][0] == bot_id
		&& ((field_[0][1] <= 0 && field_[0][2] <= 0)
			|| (field_[1][1] <= 0 && field_[2][2] <= 0)
			|| (field_[1][0] <= 0 && field_[2][0] <= 0)))
	{
		count++;
	}

	if (field_[0][1] == bot_id
		&& ((field_[0][0] <= 0 && field_[0][2] <= 0)
			|| (field_[1][1] <= 0 && field_[2][1] <= 0)))
	{
		count++;
	}

	if (field_[0][2] == bot_id
		&& ((field_[0][0] <= 0 && field_[0][1] <= 0)
			|| (field_[1][2] <= 0 && field_[2][2] <= 0)
			|| (field_[1][1] <= 0 && field_[2][0] <= 0)))
	{
		count++;
	}

	if (field_[1][0] == bot_id
		&& ((field_[0][0] <= 0 && field_[2][0] <= 0)
			|| (field_[1][1] <= 0 && field_[1][2] <= 0)))
	{
		count++;
	}

	if (field_[1][1] == bot_id
		&& ((field_[0][0] <= 0 && field_[2][2] <= 0)
			|| (field_[0][1] <= 0 && field_[2][1] <= 0)
			|| (field_[0][2] <= 0 && field_[2][0] <= 0)
			|| (field_[1][0] <= 0 && field_[1][2] <= 0)))
	{
		count++;
	}

	if (field_[1][2] == bot_id
		&& ((field_[0][2] <= 0 && field_[2][2] <= 0)
			|| (field_[1][0] <= 0 && field_[1][1] <= 0)))
	{
		count++;
	}

	if (field_[2][0] == bot_id
		&& ((field_[0][0] <= 0 && field_[1][0] <= 0)
			|| (field_[1][1] <= 0 && field_[0][2] <= 0)
			|| (field_[2][1] <= 0 && field_[2][2] <= 0)))
	{
		count++;
	}

	if (field_[2][1] == bot_id
		&& ((field_[0][1] <= 0 && field_[1][1] <= 0)
			|| (field_[2][0] <= 0 && field_[2][2] <= 0)))
	{
		count++;
	}

	if (field_[2][2] == bot_id
		&& ((field_[0][0] <= 0 && field_[1][1] <= 0)
			|| (field_[0][2] <= 0 && field_[1][2] <= 0)
			|| (field_[2][0] <= 0 && field_[2][1] <= 0)))
	{
		count++;
	}

	return count;
}

int Board::count_near_wins(int bot_id) const
{
	// 0,0 0,1 0,2
	// 1,0 1,1 1,2
	// 2,0 2,1 2,2
	int count = 0;

	// (0, 0)
	if (field_[0][0] == bot_id
		&& ((field_[0][0] == field_[0][1] && field_[0][2] <= 0)
			|| (field_[0][0] == field_[0][2] && field_[0][1] <= 0)
			|| (field_[0][0] == field_[1][0] && field_[2][0] <= 0)
			|| (field_[0][0] == field_[1][1] && field_[2][2] <= 0)
			|| (field_[0][0] == field_[2][0] && field_[1][0] <= 0)
			|| (field_[0][0] == field_[2][2] && field_[1][1] <= 0)))
	{
		count++;
	}

	// (0, 1) - (0, 0)
	if (field_[0][1] == bot_id
		&& ((field_[0][1] == field_[0][2] && field_[0][0] <= 0)
			|| (field_[0][1] == field_[1][1] && field_[2][1] <= 0)
			|| (field_[0][1] == field_[2][1] && field_[1][1] <= 0)))
	{
		count++;
	}

	// (0, 2) - (0, 0) - (0, 1)
	if (field_[0][2] == bot_id
		&& ((field_[0][2] == field_[1][1] && field_[2][0] <= 0)
			|| (field_[0][2] == field_[2][0] && field_[1][1] <= 0)
			|| (field_[0][2] == field_[1][2] && field_[2][2] <= 0)
			|| (field_[0][2] == field_[2][2] && field_[1][2] <= 0)))
	{
		count++;
	}

	// (1, 0) - (0, 0) - (0, 1) - (0, 2)
	if (field_[1][0] == bot_id
		&& ((field_[1][0] == field_[1][1] && field_[1][2] <= 0)
			|| (field_[1][0] == field_[1][2] && field_[1][1] <= 0)
			|| (field_[1][0] == field_[2][0] && field_[0][0] <= 0)))
	{
		count++;
	}

	// (1, 1) - (0, 0) - (0, 1) - (0, 2) - (1, 0)
	if (field_[1][1] == bot_id
		&& ((field_[1][1] == field_[1][2] && field_[1][0] <= 0)
			|| (field_[1][1] == field_[2][0] && field_[0][2] <= 0)
			|| (field_[1][1] == field_[2][1] && field_[0][1] <= 0)
			|| (field_[1][1] == field_[2][2] && field_[0][0] <= 0)))
	{
		count++;
	}

	// (1, 2) - (0, 0) - (0, 1) - (0, 2) - (1, 0) - (1, 1)
	if (field_[1][2] == bot_id
		&& (field_[1][2] == field_[2][2] && field_[0][2] <= 0))
	{
		count++;
	}

	// (2, 0) - (0, 0) - (0, 1) - (0, 2) - (1, 0) - (1, 1) - (1, 2)
	if (field_[2][0] == bot_id
		&& ((field_[2][0] == field_[2][1] && field_[2][2] <= 0)
			|| (field_[2][0] == field_[2][2] && field_[2][1] <= 0)))
	{
		count++;
	}

	// (2, 1) - (0, 0) - (0, 1) - (0, 2) - (1, 0) - (1, 1) - (1, 2) - (2, 0)
	if (field_[2][1] == bot_id
		&& (field_[2][1] == field_[2][2] && field_[2][0] <= 0))
	{
		count++;
	}


	return count;
}

void Board::fill(int botId)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (can_make_move(i, j))
			{
				make_move(i, j, botId);
			}
		}
	}
}

bool Board::canWinInOneMove(int botId) const
{
	// If the board is already won, tied, or lost, then we should reflect that first.
	if (get_winner() == botId)
	{
		return true;
	}

	if (get_winner() != -1)
	{
		return false;
	}

	Board simulateBoard = *this;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (simulateBoard.can_make_move(i, j))
			{
				simulateBoard.make_move(i, j, botId);

				if (simulateBoard.get_winner() == botId)
				{
					return true;
				}

				// We only test one cell at a time, so we revert to empty cell when done.
				simulateBoard.make_move(i, j, 0);
			}
		}
	}

	return false;
}

int Board::getEffectiveWinner() const
{
	// If the board is already won or tied, then return the actual winner.
	if (get_winner() != -1)
	{
		return get_winner();
	}

	Board tryPlayerOne = *this;
	Board tryPlayerTwo = *this;

	tryPlayerOne.fill(1);
	tryPlayerTwo.fill(2);

	// Player 1 can win, but player 2 can only tie.
	if (tryPlayerOne.get_winner() == 1 && tryPlayerTwo.get_winner() == 0)
	{
		return 1;
	}

	// Player 2 can win, but player 1 can only tie.
	if (tryPlayerOne.get_winner() == 0 && tryPlayerTwo.get_winner() == 2)
	{
		return 2;
	}

	// Neither player can win, so it's a tie.
	if (tryPlayerOne.get_winner() == 0 && tryPlayerTwo.get_winner() == 0)
	{
		return 0;
	}

	// Both players can win, so we don't know yet.
	return -1;
}

int Board::getMaxScore() const
{
	int max = INT_MIN;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (score_[i][j] > max && can_make_move(i, j))
			{
				max = score_[i][j];
			}
		}
	}

	return max;
}

std::pair<int, int> Board::getMove() const
{
	int max = getMaxScore();
	std::vector<std::pair<int, int>> moves;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (score_[i][j] == max && can_make_move(i, j))
			{
				// Because this function returns (column, row).
				moves.push_back(std::make_pair(j, i));
			}
		}
	}

	return moves[rand() % moves.size()];
}

bool Board::can_make_move(int row, int col) const
{
	return field_[row][col] == 0;
}

int Board::getFieldAt(int row, int col) const
{
	return field_[row][col];
}

void Board::make_move(int row, int col, int botId)
{
	field_[row][col] = botId;
}

std::pair<int, int> Board::getPosition() const
{
	return std::make_pair(col_, row_);
}

void Board::addScore(int score)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			score_[i][j] += score;
		}
	}
}

int Board::getScore(int row, int col) const
{
	return score_[row][col];
}
