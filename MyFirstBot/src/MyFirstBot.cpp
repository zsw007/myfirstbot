#include "../include/MyFirstBot.h"

std::pair<int, int> MyFirstBot::action(const std::string &type, int time)
{
	if (type != "move")
	{
		debug("Unknown action type <" + type + ">");
	}

	time_bank_ = time;

	//try
	//{
	return place_move();
	//}
	//catch (std::exception& e)
	//{
	//	debug("Exception occured, default to random algorithm <" + std::string(e.what()) + ">");
	//	return place_move_randomly();
	//}		
}

std::pair<int, int> MyFirstBot::place_move()
{
	int time_limit = std::min(time_bank_, time_per_move_);

	Move_t move = game_.calculate_best_move(time_limit);
	return std::make_pair(move.col + 3 * move.board_col, move.row + 3 * move.board_row);
}


std::pair<int, int> MyFirstBot::place_move_randomly() const
{
	return std::make_pair(0, 0);
}

void MyFirstBot::update(const std::string &player, const std::string &type, const std::string &value)
{
	if (player != "game" && player != my_name_)
	{
		debug("Unknown update player <" + player + ">");
		return;
	}

	if (type == "round")
	{
		round_ = std::stoi(value);
	}
	else if (type == "move")
	{
		move_ = std::stoi(value);
	}
	else if (type == "macroboard")
	{
		game_.update_macroboard(value);
	}
	else if (type == "field")
	{
		game_.update_boards(value);
	}
	else
	{
		debug("Unknown update type <" + type + ">.");
	}
}

void MyFirstBot::setting(const std::string &type, const std::string &value)
{
	if (type == "timebank")
	{
		time_bank_ = std::stoi(value);
	}
	else if (type == "time_per_move")
	{
		time_per_move_ = std::stoi(value);
	}
	else if (type == "player_names")
	{
		player_names_ = split(value, ',');
	}
	else if (type == "your_bot")
	{
		my_name_ = value;
	}
	else if (type == "your_botid")
	{
		bot_id_ = std::stoi(value);
		game_.set_bot_id(bot_id_);
	}
	else
	{
		debug("Unknown setting <" + type + ">.");
	}
}

MyFirstBot::MyFirstBot()
{
	time_bank_ = 10000;
	time_per_move_ = 500;
	bot_id_ = 0;
	round_ = 0;
	move_ = 0;
	my_name_ = "";
	game_ = Game();
}

void MyFirstBot::inputCommand(const std::vector<std::string> &command)
{
	if (command[0] == "action")
	{
		std::pair<int, int> point = action(command[1], std::stoi(command[2]));
		std::cout << "place_move " << point.first << " " << point.second
			<< std::endl << std::flush;
	}
	else if (command[0] == "update")
	{
		update(command[1], command[2], command[3]);
	}
	else if (command[0] == "settings")
	{
		setting(command[1], command[2]);
	}
	else
	{
		debug("Unknown command <" + command[0] + ">.");
	}
}


void MyFirstBot::debug(const std::string &s) const
{
	std::cerr << s << std::endl << std::flush;
}

int main()
{
	srand(static_cast<unsigned int>(time(nullptr)));

	MyFirstBot bot;


	std::string line;
	while (std::getline(std::cin, line))
	{
		bot.inputCommand(split(line, ' '));
	}

	/*
	bot.inputCommand({ "settings", "your_botid", "1" });
	bot.inputCommand({ "update", "game", "field", "2,1,0,0,1,0,2,0,0,0,0,0,0,0,1,0,1,0,2,1,0,2,2,0,2,0,0,1,2,0,0,0,0,0,0,1,1,0,0,0,2,2,2,0,0,1,0,0,1,0,1,2,0,0,1,2,2,0,0,1,0,0,0,0,1,0,2,2,1,0,0,0,1,2,1,0,1,0,2,2,0" });
	bot.inputCommand({"update", "game", "macroboard", "-1,-1,-1,1,-1,-1,1,-1,-1"});
	bot.inputCommand({"action", "move", "10000"});

	int i;
	std::cin >> i;
	*/


	return 0;
}
