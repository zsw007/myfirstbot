#include "../include/Stopwatch.h"

Stopwatch::Stopwatch(int limit)
{
	started_ = false;

	// Converts from miliseconds to microseconds.
	limit_ = static_cast<long long>(limit) * 1000;
}

void Stopwatch::start()
{
	if (!started_)
	{
		started_ = true;
		time_ = std::chrono::high_resolution_clock::now();
		lap_ = time_;
	}
}

void Stopwatch::stop()
{
	started_ = false;
}

long long Stopwatch::elapsed() const
{
	if (!started_)
	{
		return 0;
	}

	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
	long long duration = std::chrono::duration_cast<std::chrono::microseconds>(now - time_).count();
	return duration;
}

long long Stopwatch::lap()
{
	if (!started_)
	{
		return 0;
	}

	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
	long long duration = std::chrono::duration_cast<std::chrono::microseconds>(now - lap_).count();
	lap_ = now;
	return duration;
}

bool Stopwatch::check()
{
	if (!started_)
	{
		return false;
	}

	long long remaining = limit_ - elapsed();
	long long estimate_next = lap();

	return remaining - estimate_next > 0;
}
