#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>

class Stopwatch
{
private:

	// The maximum time limit, represented in microseconds.
	long long limit_;

	bool started_;
	std::chrono::high_resolution_clock::time_point time_;
	std::chrono::high_resolution_clock::time_point lap_;

public:

	// Initializes the Stopwatch with a maximum time limit of `limit` in miliseconds, converted to
	// microseconds, in a stopped state.
	explicit Stopwatch(int limit);

	// Starts the Stopwatch and begin tracking time elapsed.
	void start();

	// Stop the Stopwatch and stop tracking time elapsed.
	void stop();

	// Returns the total amount of time in microseconds that has elapsed since the Stopwatch 
	// started. Returns 0 if the Stopwatch is stopped.
	long long elapsed() const;

	// Returns the total amount of time in microseconds that has elapsed since the last lap(). The 
	// first lap() is assumed to have the same starting time as when the Stopwatch started. 
	// Returns 0 if the Stopwatch is stopped.
	long long lap();

	// Returns whether it is safe or not to continue another iteration, taking into consideration
	// the maximum time limit, the amount of time that elapsed so far, and the amount of time
	// that elapses per lap.
	bool check();
};

#endif
