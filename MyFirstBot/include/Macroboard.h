#ifndef MACROBOARD_H_
#define MACROBOARD_H_

#include "Board.h"

class Macroboard : public Board
{
public:
	explicit Macroboard();
	explicit Macroboard(std::vector<int> cells);

	// Returns the id of the winner of the Board. If it is a tie, 0 is returned. If the game is 
	// still ongoing, -1 is returned.
	int get_winner() const;

	// Allow all boards (that are not finished) to be played for the next move.
	void allow_play();

	// Allow only the board at (row, col) to be played for the next move.
	void allow_play(int row, int col);

	// Returns the number of free cells where a move can be made.
	int count_free_cells() const;

	// Returns whether the very next move can be played on the board at (row, col).
	bool can_play_board(int row, int col) const;

	// Returns whether future moves can be made on the board at (row, col).
	bool can_make_move(int row, int col) const;
};

#endif
