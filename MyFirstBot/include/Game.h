#ifndef GAME_H_
#define GAME_H_

#include <iostream>
#include <string>
#include <sstream>
#include "Board.h"
#include "Macroboard.h"
#include "Stopwatch.h"

typedef struct Move {
	int board_row;
	int board_col;
	int row;
	int col;
} Move_t;

class Game
{
protected:
	Board boards_[3][3];

	Macroboard macroboard_;

	// The perspective of this game.
	int bot_id_;

	int score_[3][3][3][3];

	// Returns the effective number (wins - losses) of boards won with respect to the bot whose 
	// perspective is associated with this Game.
	int count_board_effective_wins() const;

	int count_board_effective_can_wins() const;

	int count_board_effective_near_wins() const;

	int count_macroboard_effective_can_wins() const;

	// Returns the effective number (wins - losses) of "double wins" (two wins in a row that can
	// be extended into 3 in a row for a game victory) with respect to the bot whose perspective is 
	// associated with this Game.
	int count_macroboard_effective_near_wins() const;

public:
	explicit Game();

	// Calculates the best move to make and return the struct that contains the move to make.
	Move_t calculate_best_move(int time_limit);
	void update_boards(std::string value);
	void update_macroboard(std::string value);

	// Play a move from bot_id on the board designated by (board_row, board_col) on the cell
	// designated by (row, col).
	void make_move(int board_row, int board_col, int row, int col, int bot_id);

	// Returns whether the board at (row, col) is playable for the next move.
	bool can_play(int row, int col) const;

	// Returns whether the move (row, col) can be made on the board at (board_row, board_col).
	bool can_make_move(int board_row, int board_col, int row, int col) const;

	// Returns whether the game is won by the bot whose perspective is associated with this Game.
	bool is_won() const;

	// Returns whether the game is lost by the bot whose perspective is associated with this Game.
	bool is_lost() const;

	// Returns whether the game is tied.
	bool is_tied() const;

	// Returns true if either one player won the game, or the game is tied.
	bool is_game_over() const;
	
	Macroboard get_macroboard() const;

	// Gets the bot_id of the bot whose perspective is associated with this Game.
	int get_bot_id() const;

	// Sets the bot_id of our bot to associate the perspective of this Game with that of our bot.
	void set_bot_id(int bot_id);

	// Evaluates the current state of the game and returns a score that indicates how advantageous
	// of a position it is for the bot whose perspective is associated with this Game.
	int evalulate() const;
};

int alphabeta(Game game, int depth, int alpha, int beta, int turn);

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

// Calculates the id of bot_id's opponent's bot.
inline int opponent_id(int bot_id);

#endif
