#ifndef BOARD_H_
#define BOARD_H_

#include <algorithm>
#include <climits>
#include <cmath>
#include <memory>
#include <vector>

const int PLAYERONE = 1;
const int PLAYERTWO = 2;

/*
	A 3 by 3 standard Tic Tac Toe board.
*/
class Board
{
protected:
	// A 3 by 3 field where each cell contains the player that owns the cell.
	int field_[3][3];

	// A 3 by 3 field where each cell contains score for that cell.
	int score_[3][3];

	// The column of this board on the larger macroboard. 
	int col_;

	// The row of this board on the larger macroboard.
	int row_;

public:
	Board();
	explicit Board(std::vector<int> cells);
	explicit Board(std::vector<int> cells, int row, int col);

	// Returns the number of free cells where a move can be made.
	int count_free_cells() const;

	// Returns the id of the winner of the Board. If it is a tie, 0 is returned. If the game is 
	// still ongoing, -1 is returned.
	int get_winner() const;

	int count_can_wins(int bot_id) const;

	// Returns the number of "double wins" (two wins in a row that can be extended into 3 in a row 
	// for a game victory) for the bot with id of bot_id.
	int count_near_wins(int bot_id) const;

	/*
	Fill up every empty cell on the board's field with a move for "botId".
	*/
	void fill(int botId);

	/*
	Return whether or not it is possible for botId to win in 1 move from the current state of the 
	board.
	*/
	bool canWinInOneMove(int botId) const;

	/*
	Return the effective winner of the Tic Tac Toe board. That is, assuming one player plays every
	single remaining move on the board. Does that player win? Return a player's id if the only
	possibility is that either he wins or opponent ties with him, 0 if both players will tie, -1 if 
	it is too early to say either way for sure.
	*/
	int getEffectiveWinner() const;

	/*
	Get the score at "row" row and "col" column.
	*/
	int getScore(int row, int col) const;

	/*
	Get the maximum score out of all cells where a move can be made.
	*/
	int getMaxScore() const;

	/*
	Return the (column, row) coordinates of the cell with the highest score that is also playable. 
	If there is a tie, pick a random cell between tied cells.
	*/
	std::pair<int, int> getMove() const;

	/*
	Make a move at the "row" row and "col" column of this board that belongs to "botId".
	*/
	void make_move(int row, int col, int botId);

	/*
	Return the (col, row) position of this board in the grand macroboard.
	*/
	std::pair<int, int> getPosition() const;

	/*
	Add to score of this board by incrementing the score of each cell by the value of "score".
	*/
	void addScore(int score);

	/*
	Return whether or not it is possible to make a move at the "row" row and "col" column of this
	board.
	*/
	bool can_make_move(int row, int col) const;

	/*
	Return the move made on the field at position ("row", "col").
	*/
	int getFieldAt(int row, int col) const;
};

/*
Given your botId, calculate the id of your opponent.
*/
inline int calculateOpponentId(int botId);

/*
Simulate making a move at the "row" row and "col" column of "board" by "botId" and return the
resulting board.
*/
Board simulateMove(Board board, int row, int col, int botId);

/*
Calculate the score of every cell in the given "board" from the perspective of "botId". Return the
score for every cell.
*/
std::vector<std::vector<int>> calculateScore(Board board, int botId);

/*
Calculate the max score of the given "board" arrangement from the perspective of "botId", if the
next move is to be made by "turnPlayer" and this is currently the "depth"th move in this 
calculation. If we "alternate", then we assume the move after the next move will be made by the
opponent, and so on. otherwise, we assume "turnPlayer" will make every single move.
*/
int minimax(Board board, int botId, int turnPlayer, int depth);
int minimax(Board board, int botId, int turnPlayer);

#endif
