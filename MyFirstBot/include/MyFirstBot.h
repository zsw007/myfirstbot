#ifndef MYFIRSTBOT_H_
#define MYFIRSTBOT_H_

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <memory>
#include <exception>
#include "Board.h"
#include "Game.h"

/*
	The AI Games (http://theaigames.com/) Tic Tac Toe Ultimate entry.
	@author Shuowang "Wayne" Zhang
*/
class MyFirstBot
{
protected:

	// static settings
	int time_bank_;
	int time_per_move_;

	// The id of our bot.
	int bot_id_;
	std::vector<std::string> player_names_;
	std::string my_name_;

	// dynamic settings
	int round_;
	int move_;

	Game game_;


	std::pair<int, int> action(const std::string &type, int time);

	/*
	The main algorithm. A series of criteria will be checked for each avaliable cell. Each
	criteria will either gain score or lose score for that cell. When all criterias are
	checked, select the cell with the highest score. If there is a tie between cells, a random
	cell is selected between the tied cells.
	*/
	std::pair<int, int> place_move();

	/*
	The random algorithm. Randomly selects a cell out of avaliable cells.

	Only used as a last resort to prevent disqualification in the unlikely event that an
	exception occurs when running the main algorithm, or the timebank is depleted.

	To be disabled during testing.
	*/
	std::pair<int, int> place_move_randomly() const;

	void update(const std::string &player, const std::string &type, const std::string &value);

	void setting(const std::string &type, const std::string &value);

	void debug(const std::string &s) const;

public:

	/*
	Initializes MyFirstBot.
	*/
	MyFirstBot();

	void inputCommand(const std::vector<std::string> &command);
};

#endif
